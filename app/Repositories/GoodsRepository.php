<?php


namespace App\Repositories;

use App\Models\Goods;
use Illuminate\Http\Request;

class GoodsRepository extends BaseRepository
{
    public function getIndex()
    {
        return Goods::all();
    }

    public function getShow($id)
    {
        return Goods::find($id);
    }
    public function getStore(Request $request)
    {
        $data = $this->validates($request,[
            'product_name' => 'required',
            'product_size' => 'required',
            'product_code' => 'required',
            'product_in_warehouse' =>'required',
            'new_product' =>'required',
            'rent_product' =>'required'
        ]);

        $check_goods = Goods::where('product_code',$data['product_code'])->count();

        if($check_goods>0){
            abort( response()->json(["error"=>"Product already exist"], 200) );
        }

        return Goods::create($data);

    }
    public function getUpdate(Request $request,$id)
    {
        $data = $this->validates($request,[
            'product_code' => 'required'
        ]);

        $goods = Goods::find($id);
        if (is_null($goods)){
            abort( response()->json(["error"=>"Product do not  exist"], 200) );
        }

        $goods->fill($data);
        $goods->save();

        return $goods;
    }

    public function getDelete($id)
    {
        $goods = Goods::find($id);
        if (is_null($goods)){
            abort( response()->json(["error"=>"Product do not  exist"], 200) );
        }

        $goods->delete();

        return $goods;
    }

}
