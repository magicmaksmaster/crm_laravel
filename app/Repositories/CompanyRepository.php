<?php


namespace App\Repositories;

use App\Models\Company;
use Illuminate\Http\Request;


class CompanyRepository extends BaseRepository
{
      public function getIndex()
    {
        $company = Company::with('goods')->get();
        return $company;
    }

    public function getShow($id)
    {
        return Company::find($id);
    }
    public function getStore(Request $request)
    {
        $data = $this->validates($request,[
            'name_company' => 'required',
            'email_company' => 'required|email:rfc,dns',
            'password_company' => 'required',
            'phone_company' =>'required'
        ]);

        $check_company = Company::where('email_company',$data['email_company'])->count();

        if($check_company>0){
            abort( response()->json(["error"=>"Company already exist"], 200) );
        }

        return Company::create($data);

    }
    public function getUpdate(Request $request,$id)
    {
        $data = $this->validates($request,[
            'email_company' => 'required|email:rfc,dns'
        ]);

        $company = Company::find($id);
        if (is_null($company)){
            abort( response()->json(["error"=>"Company do not  exist"], 200) );
        }

        $company->fill($data);
        $company->save();

        return $company;
    }

    public function getDelete($id)
    {
        $company = Company::find($id);
        if (is_null($company)){
            abort( response()->json(["error"=>"Company do not  exist"], 200) );
        }

        $company->delete();

        return $company;
    }

}
