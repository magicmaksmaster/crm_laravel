<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Http\Request;


class UserRepository extends BaseRepository
{

    public function getIndex()
    {
        $user = User::with('company')->get();
        return $user;
    }

    public function getShow($id)
    {
        return User::find($id);
    }

    public function getStore(Request $request)
    {
        $data = $this->validates($request,[
            'name' => 'required',
            'email' => 'required|email:rfc,dns',
            'password' => 'required'
        ]);

        $check_user = User::where('email',$data['email'])->count();

        if($check_user>0){
            abort( response()->json(["error"=>"User already exist"], 200) );
        }

        $user = User::create($data);

        return $user;
    }

    public function getUpdate(Request $request,$id)
    {
        $data = $this->validates($request,[
            'email' => 'required|email:rfc,dns'
        ]);

        $user = User::find($id);
        if (is_null($user)){
            abort( response()->json(["error"=>"User do not  exist"], 200) );
        }

        $user->fill($data);
        $user->save();

        return $user;
    }

    public function getDelete($id)
    {
        $user = User::find($id);
        if (is_null($user)){
            abort( response()->json(["error"=>"User do not  exist"], 200) );
        }

        $user->delete();

        return $user;
    }

}
