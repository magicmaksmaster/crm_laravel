<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BaseRepository
{
    public function validates(Request $data,$rule)
    {
        $input = $data->all();
        $validator = Validator::make($input,$rule);
        if( $validator->fails() ) {
            abort( response()->json($validator->errors(), 200) );
        }

        return $input;
    }


}
