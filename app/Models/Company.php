<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Goods;

class Company extends Authenticatable
{
    public function goods()
    {
        return $this->hasMany(Goods::class, 'parent_id_goods','id');
    }
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'company';

    protected $fillable = [
        'name_company',
        'phone_company',
        'email_company',
        'password_company',

    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


}
