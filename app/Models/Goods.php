<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as Authenticatable ;


class Goods extends Authenticatable
{
    use HasFactory;

    protected $table = 'goods';

    protected $fillable = [
        'product_name',
        'product_size',
        'product_code',
        'product_in_warehouse',
        'new_product',
        'rent_product'

    ];



}
