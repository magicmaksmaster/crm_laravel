<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    public function __construct()
    {
    }

    public function sendResponse($data, $massage, $status=true)
    {
        $result = [
            "data"=> $data,
            "massage" => $massage,
            "status" => $status
        ];

        return response()->json($result);
    }

//    public function validates(Request $data,$rule)
//    {
//        $input = $data->all();
//        $validator = Validator::make($input,$rule);
//        if( $validator->fails() ) {
//            abort( response()->json($validator->errors(), 200) );
//        }
//
//        return $input;
//    }

}
