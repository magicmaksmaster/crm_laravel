<?php

namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use App\Repositories\UserRepository;

class UserController extends BaseController
{
    private $UserRepository;

    public function __construct()
    {
        parent::__construct();
        $this->UserRepository = app(UserRepository::class);
    }

    public function index()
   {
       $users = $this->UserRepository->getIndex();

       return $this->sendResponse($users,'User find!');
   }
   public function show($id)
   {
       $user = $this->UserRepository->getShow($id);

       return $this->sendResponse($user,'User find!');
   }
   public function store(Request $request)
   {
       $user = $this->UserRepository->getStore($request);

       return $this->sendResponse($user,'User find!');
   }
   public function update(Request $request,$id)
   {
       $user = $this->UserRepository->getUpdate($request,$id);

       return $this->sendResponse($user,'User find!');
   }
    public function destroy($id)
    {
        $user = $this->UserRepository->getDelete($id);

        return $this->sendResponse($user,'User delete!');
    }
}
