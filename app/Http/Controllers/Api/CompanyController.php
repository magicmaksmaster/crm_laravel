<?php


namespace App\Http\Controllers\Api;


use App\Repositories\CompanyRepository;
use Illuminate\Http\Request;




class CompanyController extends BaseController
{
    private $CompanyRepository;

    public function __construct()
    {
        parent::__construct();
        $this->CompanyRepository = app(CompanyRepository::class);
    }

    public function index()
    {
        $company = $this->CompanyRepository->getIndex();

        return $this->sendResponse($company,'Company find!');
    }
    public function show($id)
    {
        $company = $this->CompanyRepository->getShow($id);

        return $this->sendResponse($company,'Company find!');
    }
    public function store(Request $request)
    {
        $company = $this->CompanyRepository->getStore($request);

        return $this->sendResponse($company,'Company find!');
    }
    public function update(Request $request,$id)
    {
        $company = $this->CompanyRepository->getUpdate($request,$id);

        return $this->sendResponse($company,'Company find!');
    }
    public function destroy($id)
    {
        $company = $this->CompanyRepository->getDelete($id);

        return $this->sendResponse($company,'Company delete!');
    }
}
