<?php


namespace App\Http\Controllers\Api;

use App\Repositories\GoodsRepository;
use Illuminate\Http\Request;

class GoodsController extends BaseController
{
    private $GoodsRepository;

    public function __construct()
    {
        parent::__construct();
        $this->GoodsRepository = app(GoodsRepository::class);
    }

    public function index()
    {
        $goods = $this->GoodsRepository->getIndex();

        return $this->sendResponse($goods,'Goods find!');
    }
    public function show($id)
    {
        $goods = $this->GoodsRepository->getShow($id);

        return $this->sendResponse($goods,'Goods find!');
    }
    public function store(Request $request)
    {
        $goods = $this->GoodsRepository->getStore($request);

        return $this->sendResponse($goods,'Goods find!');
    }
    public function update(Request $request,$id)
    {
        $goods = $this->GoodsRepository->getUpdate($request,$id);

        return $this->sendResponse($goods,'Goods find!');
    }
    public function destroy($id)
    {
        $goods = $this->GoodsRepository->getDelete($id);

        return $this->sendResponse($goods,'Goods delete!');
    }

}
