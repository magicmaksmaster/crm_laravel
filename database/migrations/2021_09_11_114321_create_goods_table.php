<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->integer('parent_id_goods');
            $table->string('product_name');
            $table->string('product_size');
            $table->string('product_code');
            $table->integer('product_in_warehouse');
            $table->integer('new_product');
            $table->integer('rent_product');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods');
    }
}
